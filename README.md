# Webman

A new way of look at man for a command. When the man command look for the manual inside a local db, webman open a browser tab with the link to the documentation for the command.

---

### Usage
Init: create your webmans
```bash
webman add -k example -u https://wwww.google.com
```

Search for a webman and open it (interactive mode)
```bash
webman
```

List all webmans
```bash
webman ls
```

Remove a webman
```bash
webman rm -k example
```

## Development
Use `run.sh` script to compile an executable, then run it
```bash
./run.sh
```

To run commands you can use this as a template
```bash
./run add -k example -u https://wwww.google.com
```

## Build release
Run the script `./build_releases.sh`. All generated executables will be in folder `./build`

Architectures supported:
```
darwin amd64
darwin arm64
freebsd 386
freebsd amd64
freebsd arm
linux 386
linux amd64
linux arm
linux arm64
windows 386
windows amd64
```

## Install the command in Go folder
```bash
./install.sh
```
In order to run the command nativly shell environemnt variables `GOROOT` and `GOPATH` must be set.

`GOROOT` points to the actual Go binary.

`GOPATH` points to the Go folder.