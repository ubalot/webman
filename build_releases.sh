#!/usr/bin/env bash

version=$(git tag -l | tail -n 1)
last_commit_hash=$(git log --oneline -n 1 | cut -d' ' -f 1)

build_path=build
test -d "$build_path" && rm -rf "$build_path"/* || mkdir -p "$build_path"

declare -a targets=(
"darwin amd64"
"darwin arm64"
"freebsd 386"
"freebsd amd64"
"freebsd arm"
"linux 386"
"linux amd64"
"linux arm"
"linux arm64"
"windows 386"
"windows amd64"
)

for i in "${targets[@]}"
do
    os=$(echo "$i" | cut -f 1 -d' ')
    target=$(echo "$i" | cut -f 2 -d' ')
    bin="webman_${version}_${last_commit_hash}_${os}_${target}"
    env GOROOT="" GOOS="$os" GOARCH="$target" go build -o "${build_path}/${bin}"
    if [ $? -eq 0 ]
    then
        echo "file $bin created successfully"
    else
        echo "building of file $bin failed!!!  os: $os  target: $target"
    fi
done
