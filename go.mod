module gitlab.com/ubalot/webman

go 1.18

require (
	github.com/c-bata/go-prompt v0.2.6
	github.com/pkg/browser v0.0.0-20210911075715-681adbf594b8
)

require (
	github.com/kirsle/configdir v0.0.0-20170128060238-e45d2f54772f
	github.com/mattn/go-colorable v0.1.7 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/mattn/go-tty v0.0.3 // indirect
	github.com/pkg/term v1.2.0-beta.2 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	golang.org/x/sys v0.0.0-20211019181941-9d821ace8654 // indirect
)
