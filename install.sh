#!/usr/bin/env bash

set -e

env GOROOT="" go env -w GOBIN=~/go/bin
env GOROOT="" go install
