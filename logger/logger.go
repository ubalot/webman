package logger

import (
	"fmt"
	"log"
	"os"

	ut "gitlab.com/ubalot/webman/utilities"
)

type Log struct {
	LogFile string
	MaxSize int64
	logger  *log.Logger
	file    *os.File
}

func (logger *Log) init() {
	if len(logger.LogFile) == 0 {
		fmt.Println("empty log file")
		os.Exit(1)
	}
	if logger.logger == nil {
		var file *os.File
		var err error
		if ut.FileExists(logger.LogFile) {
			file, err = os.OpenFile(logger.LogFile, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)
		} else {
			file, err = os.Create(logger.LogFile)
		}
		if err != nil {
			panic(err)
		}
		logger.file = file
		logger.logger = log.New(file, "", log.Ldate|log.Ltime|log.Lshortfile)
	}
}

func (logger *Log) Write(msg string) {
	logger.init()
	logger.logger.Print(msg)
}

func (logger *Log) Writeln(msg string) {
	logger.init()
	logger.logger.Println(msg)
}

func (logger *Log) revertFileContent() {
	data, err := os.ReadFile(logger.LogFile)
	if err != nil {
		panic(err)
	}
	err = os.WriteFile(logger.LogFile, ut.ReversedArray(data), 0666)
	if err != nil {
		panic(err)
	}
}

func (logger *Log) truncate() {
	logger.revertFileContent()
	err := os.Truncate(logger.LogFile, logger.MaxSize)
	if err != nil {
		panic(err)
	}
	logger.revertFileContent()
}

// Must be called before program exit (because file descriptor should be closed)
func (logger *Log) CheckSize() {
	fi, err := os.Stat(logger.LogFile)
	if err != nil {
		panic(err)
	}
	size := fi.Size()
	if size > logger.MaxSize {
		logger.truncate()
	}
	logger.file.Close()
}
