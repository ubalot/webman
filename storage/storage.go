package storage

import (
	"os"

	ut "gitlab.com/ubalot/webman/utilities"
)

type Storage struct {
	Filename string
}

func (storage *Storage) init() error {
	err := os.WriteFile(storage.Filename, []byte("[]\n"), 0644)
	if err != nil {
		return err
	}
	return nil
}

func (storage *Storage) check() error {
	if !ut.FileExists(storage.Filename) {
		err := storage.init()
		if err != nil {
			return err
		}
	}
	return nil
}

func (storage *Storage) Read() ([]byte, error) {
	err := storage.check()
	if err != nil {
		return nil, err
	}

	data, err := os.ReadFile(storage.Filename)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (storage *Storage) Save(content []byte) error {
	err := storage.check()
	if err != nil {
		return err
	}

	err = os.WriteFile(storage.Filename, content, 0644)
	if err != nil {
		return err
	}
	return nil
}
