package utilities

import (
	"errors"
	"fmt"
	"os"
)

func FileExists(filename string) bool {
	_, err := os.Stat(filename)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) { // file doesn't exist
			return false
		} else { // Schrodinger: file may or may not exist. See err for details.
			fmt.Println(err)
			return false
		}
	}
	return true
}

func ReversedArray[T any](arr []T) []T {
	reversed := make([]T, len(arr))
	for i := len(arr) - 1; i >= 0; i-- {
		reversed[len(arr)-1-i] = arr[i]
	}
	return reversed
}
