package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"time"

	prompt "github.com/c-bata/go-prompt"
	"github.com/kirsle/configdir"
	"github.com/pkg/browser"
	"gitlab.com/ubalot/webman/logger"
	"gitlab.com/ubalot/webman/storage"
	"gitlab.com/ubalot/webman/webman"
)

const (
	programName = "webman"
	jsonFile    = "webmans.json"
	logFile     = programName + ".log"
)

// Workaround for a problem about go-prompt. When it exit, no input can be viewed on terminal.
// This is the solution proposed on github issues:
//    https://github.com/c-bata/go-prompt/issues/228#issuecomment-820639887
func handleExit() {
	rawModeOff := exec.Command("/bin/stty", "-raw", "echo")
	rawModeOff.Stdin = os.Stdin
	_ = rawModeOff.Run()
	rawModeOff.Wait()
}

func getConfigPath(appName string) string {
	configPath := configdir.LocalConfig("webman")
	err := configdir.MakePath(configPath)
	if err != nil {
		panic(err)
	}
	return configPath
}

func interactivePrompt(webmans webman.Webmans) error {
	defer handleExit()

	configPath := getConfigPath(programName)
	logFile := filepath.Join(configPath, logFile)
	applog := logger.Log{LogFile: logFile, MaxSize: 100000} //100k

	keyword := prompt.Input("> ", webmans.Completer)
	applog.Writeln("user input : " + keyword)
	if webmans.KeywordExists(keyword) {
		url, _ := webmans.GetUrl(keyword)
		go func() {
			applog.Writeln("app :: browser open url : " + url)
			err := browser.OpenURL(url)
			if err != nil {
				panic(err)
			}
		}()
		time.Sleep(time.Second * 2) // wait for browser to open
		applog.CheckSize()
		return nil
	} else {
		msg := "app :: no match found"
		applog.Writeln(msg)
		applog.CheckSize()
		return errors.New(msg)
	}
}

func main() {
	configPath := getConfigPath(programName)
	storageFile := filepath.Join(configPath, jsonFile)
	storage := storage.Storage{Filename: storageFile}
	data, err := storage.Read()
	if err != nil {
		panic(err)
	}
	var webmans webman.Webmans
	err = json.Unmarshal(data, &webmans)
	if err != nil {
		panic(err)
	}

	const (
		flagAddKeyValue = "" // default value
		flagAddKeyUsage = "keyword"

		flagAddUrlValue = "" // default value
		flagAddUrlUsage = "url"
	)

	var addKey, addUrl string
	addCmd := flag.NewFlagSet("add", flag.ExitOnError)
	addCmd.StringVar(&addKey, "key", flagAddKeyValue, flagAddKeyUsage)
	addCmd.StringVar(&addKey, "k", flagAddKeyValue, flagAddKeyUsage+" (shorthand)")
	addCmd.StringVar(&addUrl, "url", flagAddUrlValue, flagAddUrlUsage)
	addCmd.StringVar(&addUrl, "u", flagAddUrlValue, flagAddUrlUsage+" (shorthand)")

	var rmKey string
	rmCmd := flag.NewFlagSet("rm", flag.ExitOnError)
	rmCmd.StringVar(&rmKey, "key", flagAddKeyValue, flagAddKeyUsage)
	rmCmd.StringVar(&rmKey, "k", flagAddKeyValue, flagAddKeyUsage+" (shorthand)")

	lsCmd := flag.NewFlagSet("ls", flag.ExitOnError)

	if len(os.Args) == 1 {
		err := interactivePrompt(webmans)
		if err != nil {
			os.Exit(0)
		}
	} else {
		switch os.Args[1] {
		case "add":
			addCmd.Parse(os.Args[2:])
			if len(addKey) == 0 {
				fmt.Println("empty key argument.")
				os.Exit(3)
			}
			if len(addUrl) == 0 {
				fmt.Println("empty url argument.")
				os.Exit(3)
			}

			if !webmans.KeywordExists(addKey) {
				webmans = append(webmans, webman.Webman{Keyword: addKey, Url: addUrl})
				err = storage.Save(webmans.Prettify())
				if err != nil {
					panic(err)
				}
			}

		case "rm":
			rmCmd.Parse(os.Args[2:])
			if len(rmKey) == 0 {
				fmt.Println("empty key argument.")
				os.Exit(3)
			}

			length := len(webmans)
			webmans = webmans.Remove(rmKey)
			if length != len(webmans) {
				err = storage.Save(webmans.Prettify())
				if err != nil {
					panic(err)
				}
			}

		case "ls":
			lsCmd.Parse(os.Args[2:])
			for _, wm := range webmans {
				fmt.Println("keyword: " + wm.Keyword + " , url: " + wm.Url)
			}

		default:
			fmt.Println("expected 'add' subcommand or nothing at all")
			os.Exit(2)
		}
	}
}
