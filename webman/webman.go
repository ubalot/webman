package webman

import (
	"encoding/json"
	"errors"
	"regexp"

	"github.com/c-bata/go-prompt"
)

type Webman struct {
	Keyword string `json:"keyword"`
	Url     string `json:"url"`
}

type Webmans []Webman

func (webmans *Webmans) KeywordExists(keyword string) bool {
	for _, webman := range *webmans {
		if webman.Keyword == keyword {
			return true
		}
	}
	return false
}

func (webmans *Webmans) Keywords() []string {
	var keywords []string
	for _, webman := range *webmans {
		keywords = append(keywords, webman.Keyword)
	}
	return keywords
}

func (webmans *Webmans) GetUrl(keyword string) (string, error) {
	for _, webman := range *webmans {
		if webman.Keyword == keyword {
			return webman.Url, nil
		}
	}
	return "", errors.New("keyword doesn't exist")
}

func (webmans *Webmans) Remove(keyword string) Webmans {
	var filteredWebmans Webmans
	for _, wm := range *webmans {
		if wm.Keyword != keyword {
			filteredWebmans = append(filteredWebmans, wm)
		}
	}
	return filteredWebmans
}

func (webmans *Webmans) Prettify() []byte {
	out, err := json.MarshalIndent(&webmans, "", "  ")
	if err != nil {
		return nil
	}
	return out
}

func (webmans *Webmans) Completer(d prompt.Document) []prompt.Suggest {
	var (
		suggestions        []prompt.Suggest
		similarSuggestions []prompt.Suggest
	)
	input := d.GetWordBeforeCursor()
	if len(input) > 0 {
		r, _ := regexp.Compile(input)
		for _, keyword := range webmans.Keywords() {
			if r.MatchString(keyword) {
				suggestions = append(suggestions, prompt.Suggest{Text: keyword})
			}
		}
		suggestions = append(suggestions, similarSuggestions...)
	}
	return prompt.FilterHasPrefix(suggestions, "" /* accept any suggestion */, true)
}
